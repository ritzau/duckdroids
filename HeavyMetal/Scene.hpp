//
//  Scene.hpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-24.
//

#pragma once

#include <array>
#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <simd/simd.h>

#include "Matrix.h"
#include "ShaderTypes.hpp"

namespace tsn {

class Node;

using duration = std::chrono::milliseconds;
using time_point = std::chrono::time_point<std::chrono::high_resolution_clock, duration>;

class Transform {
    matrix_float4x4 matrix_;

public:
    Transform() : matrix_{matrix_identity_float4x4} {
    }

    Transform& operator=(Transform other) {
        using std::swap;
        swap(matrix_, other.matrix_);
        return *this;
    }

    auto& set(matrix_float4x4 matrix) {
        matrix_ = matrix;
        return *this;
    }

    auto& reset() {
        matrix_ = matrix_identity_float4x4;
        return *this;
    }

    auto& translate(float dx, float dy, float dz = 0) {
        auto translationMatrix = matrix4x4_translation(dx, dy, dz);
        matrix_ = matrix_multiply(matrix_, translationMatrix);
        return *this;
    }

    auto& rotate(float radians, float ax, float ay, float az) {
        auto rotation_matrix = matrix4x4_rotation(radians, (simd_float3) {ax, ay, az});
        matrix_ = matrix_multiply(matrix_, rotation_matrix);
        return *this;
    }

    auto& scale(float sx, float sy, float sz) {
        auto scale_matrix = matrix4x4_scale(sx, sy, sz);
        matrix_ = matrix_multiply(matrix_, scale_matrix);
        return *this;
    }

    auto& scale(float s) {
        return scale(s, s, s);
    }

    auto matrix() const noexcept {
        return matrix_;
    }
};

class Scene;

std::vector<Node*> filter_collisions(
    const std::vector<std::pair<tsn::Node*, tsn::Node*>>& collisions,
    Node *with_node);

struct Behaviour {
    virtual ~Behaviour() {
    }

    virtual void update(
        Scene *scene,
        simd_float4x4 global_transform,
        Node *node,
        time_point time,
        const std::vector<std::pair<tsn::Node*, tsn::Node*>>& collisions)
    {
        update(scene, global_transform, node, time, filter_collisions(collisions, node));
    }

    virtual void update(
        Scene *scene,
        simd_float4x4 global_transform,
        Node *node,
        time_point time,
        const std::vector<tsn::Node*>& collisions)
    { }

};

class Node {
    std::string name_;
    float collision_radius_;
    std::shared_ptr<Behaviour> behaviour_;
    std::vector<std::shared_ptr<Node>> modified_children_;
    bool child_list_modified_;
    friend class Scene;

protected:
    std::vector<std::shared_ptr<Node>> children_;

public:
    Transform transform;

    Node(std::string name, float collision_radius, std::shared_ptr<Behaviour> behaviour)
     : name_{std::move(name)}, collision_radius_{collision_radius}, behaviour_{std::move(behaviour)}, child_list_modified_{false}
    { }

    virtual ~Node() {
    }

    auto behaviour() const noexcept {return behaviour_;}
    auto name() const noexcept {return name_;}

    void add_child(std::shared_ptr<Node> child) {
        if (!child_list_modified_) {
            modified_children_ = children_;
            child_list_modified_ = true;
        }

        modified_children_.push_back(std::move(child));
    }

    void delete_child(const Node *child) {
        if (!child_list_modified_) {
            modified_children_ = children_;
            child_list_modified_ = true;
        }

        auto hit = std::find_if(modified_children_.begin(), modified_children_.end(), [child](auto c) {return child == c.get();});
        if (hit != modified_children_.end()) {
            modified_children_.erase(hit);
        }
    }

    void update(Scene *scene, time_point time, simd_float4x4 parent_transform, const std::vector<std::pair<Node*, Node*>>& collisions) {
        auto global_transform = simd_mul(parent_transform, transform.matrix());

        if (child_list_modified_) {
            children_.swap(modified_children_);
            modified_children_.clear();
            child_list_modified_ = false;
        }

        if (behaviour_) behaviour_->update(scene, global_transform, this, time, collisions);

        for (auto &child : children_) {
            child->update(scene, time, global_transform, collisions);
        }
    }
};

class Scene {
    matrix_float4x4 projection_matrix_;
    matrix_float4x4 view_matrix_;

    std::shared_ptr<Node> root_;

public:
    // FIXME
    std::vector<Light> lights_;


    Scene(std::shared_ptr<Node>&& root, std::vector<Light>&& lights)
    : root_{std::move(root)},
    lights_{lights},
    projection_matrix_{matrix_identity_float4x4},
    view_matrix_{matrix_multiply(matrix4x4_translation(0.0, 0.0, -20.0), matrix4x4_rotation(M_PI/4, (simd_float3) {1.0, 0.0, 0.0}))}
    { }

    void update_dimensions(int width, int height) {
        auto aspect = static_cast<float>(width)/static_cast<float>(height);
        projection_matrix_ = matrix_perspective_right_hand(65.0f * (M_PI / 180.0f), aspect, 0.1f, 100.0f);
    }

    auto camera_world_position() const noexcept {
        Transform cam;
        return get_translation(cam.set(view_matrix_).scale(1, 1, -1).matrix());
    }

    auto projection_matrix() const noexcept {return projection_matrix_;}

    auto view_matrix() const noexcept {return view_matrix_;}

    auto ambient_light_color() const noexcept {return simd_float3{0.1, 0.1, 0.1};}

    auto root() const noexcept {return root_;}

    template<typename T>
    void update(T time) {
        auto collisions = detect_collisions();
        root_->update(this, std::chrono::time_point_cast<duration>(time), matrix_identity_float4x4, collisions);
    }

    template<typename N, typename ...Args>
    void render(Args ...args) {
        dynamic_cast<N*>(root_.get())->render(this, args...);
    }

    std::vector<std::pair<Node*, Node*>> detect_collisions() {
        auto collisions = std::vector<std::pair<Node*, Node*>>{};
        auto nodes = std::vector<std::pair<Node*, simd_float3>>{};

        walk_nodes(std::back_inserter(nodes), matrix_identity_float4x4, root_.get());

        for (auto i = nodes.cbegin(); i != nodes.cend(); ++i) {
            auto [n0, v0] = *i;
            for (auto j = i + 1; j != nodes.cend(); ++j) {
                auto [n1, v1] = *j;
                auto d2 = simd_distance_squared(v0, v1);
                auto r = n0->collision_radius_ + n1->collision_radius_;

                if (r * r > d2) {
                    collisions.push_back({n0, n1});
                }
            }
        }

        return collisions;
    }

private:
    template<typename Out>
    void walk_nodes(Out out, simd_float4x4 parent_matrix, Node *n) {
        if (n == nullptr) return;

        auto m = simd_mul(parent_matrix, n->transform.matrix());

        if (n->collision_radius_ > 0) {
            *out++ = {n, simd_make_float3(m.columns[3])};
        }

        for (auto c : n->children_) {
            walk_nodes(out, m, c.get());
        }
    }
};

}
