//
//  MetalNodeBuilder.h
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-30.
//

#pragma once

#include <memory>
#include <functional>

#include "Scene.hpp"

namespace tsn {
namespace metal {

enum class AssetKind {
    None, Bob, Blub
};

class MetalAssetLibrary;

class AssetLibrary {
    MetalAssetLibrary* metal_library_;

public:
    AssetLibrary(MetalAssetLibrary *metal_library)
    : metal_library_{metal_library}
    { }

    std::unique_ptr<Node> make_node(std::string name, float collision_radius, AssetKind asset_kind, std::shared_ptr<Behaviour> behaviour);
    std::unique_ptr<Node> make_node(std::string name, float collision_radius = 0, AssetKind asset_kind = AssetKind::None);
};

}
}
