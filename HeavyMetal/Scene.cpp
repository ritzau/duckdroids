//
//  Scene.cpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-24.
//

#include "Scene.hpp"

namespace tsn {

std::vector<Node*> filter_collisions(
    const std::vector<std::pair<Node*, Node*>>& collisions,
    Node *with_node)
{
    auto matching = std::vector<std::pair<tsn::Node*, tsn::Node*>>{};
    std::copy_if(collisions.cbegin(), collisions.cend(), std::back_inserter(matching), [with_node](auto pair) {
        return std::get<0>(pair) == with_node || std::get<1>(pair) == with_node;
    });

    auto nodes = std::vector<Node*>{};
    std::transform(matching.cbegin(), matching.cend(), std::back_inserter(nodes), [with_node](auto pair) {
        return with_node == std::get<0>(pair) ? std::get<1>(pair) : std::get<0>(pair);
    });

    return nodes;
}

}
