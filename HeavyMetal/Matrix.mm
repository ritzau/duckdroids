//
//  matrix.m
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-25.
//

#include "Matrix.h"

matrix_float4x4 matrix4x4_translation(float tx, float ty, float tz)
{
    return simd_float4x4 {{
        { 1,   0,  0,  0 },
        { 0,   1,  0,  0 },
        { 0,   0,  1,  0 },
        { tx, ty, tz,  1 }
    }};
}

matrix_float4x4 matrix4x4_scale(float sx, float sy, float sz)
{
    return simd_float4x4 {{
        { sx, 0,  0,  0 },
        { 0,  sy, 0,  0 },
        { 0,  0,  sz, 0 },
        { 0,  0,  0,  1 }
    }};
}

matrix_float4x4 matrix4x4_rotation(float radians, simd_float3 axis)
{
    axis = vector_normalize(axis);
    float ct = cosf(radians);
    float st = sinf(radians);
    float ci = 1 - ct;
    float x = axis.x, y = axis.y, z = axis.z;

    return simd_float4x4 {{
        { ct + x * x * ci,     y * x * ci + z * st, z * x * ci - y * st, 0},
        { x * y * ci - z * st,     ct + y * y * ci, z * y * ci + x * st, 0},
        { x * z * ci + y * st, y * z * ci - x * st,     ct + z * z * ci, 0},
        {                   0,                   0,                   0, 1}
    }};
}

matrix_float4x4 matrix_perspective_right_hand(float fovyRadians, float aspect, float nearZ, float farZ)
{
    float ys = 1 / tanf(fovyRadians * 0.5);
    float xs = ys / aspect;
    float zs = farZ / (nearZ - farZ);

    return simd_float4x4 {{
        { xs,   0,          0,  0 },
        {  0,  ys,          0,  0 },
        {  0,   0,         zs, -1 },
        {  0,   0, nearZ * zs,  0 }
    }};
}

simd_float3 get_translation(simd_float4x4 matrix) {
    return simd_make_float3(matrix.columns[3]);
}

simd_float3x3 normal_matrix_of(simd_float4x4 matrix) {
    auto upper_left = simd_float3x3 {
        simd_make_float3(matrix.columns[0]),
        simd_make_float3(matrix.columns[1]),
        simd_make_float3(matrix.columns[2])};
    auto transposed = simd_transpose(upper_left);
    auto inversed = simd_inverse(transposed);

    return inversed;
}
