//
//  Duckroids.cpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-30.
//

#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <iostream>
#include <random>

#include "Duckroids.hpp"

#include "Input.hpp"
#include "ShaderTypes.hpp"

using namespace std::chrono_literals;
using namespace std::string_literals;

namespace duck {

static auto& duck_random_generator() {
    static std::random_device random_device;
    static std::mt19937 random_generator{random_device()};

    return random_generator;
}

class BulletBehaviour;

class PlanetBehaviour : public tsn::Behaviour {
    const float radius_;

    float scale_;
    float rotation_;
    int hits_ = 0;

public:
    PlanetBehaviour(float scale, float radius, float rotation = 0)
     : scale_{scale}, radius_{radius}, rotation_{rotation}
    { }

    void update(
        tsn::Scene *scene,
        simd_float4x4 global_transform,
        tsn::Node *node,
        tsn::time_point time,
        const std::vector<tsn::Node*>& collisions)
    override {
        auto hit_by_bullet = std::any_of(collisions.cbegin(), collisions.cend(), [](auto n) {
            return dynamic_cast<BulletBehaviour*>(n->behaviour().get()) != nullptr;
        });

        if (hit_by_bullet) {
            scale_ /= 2;
            if (hits_++ > 3) {
                scene->root()->delete_child(node);
            }
        }

        node->transform.reset()
            .rotate(rotation_, 0, 1, 0)
            .translate(radius_, 0, 0)
            .scale(4 * scale_)
            .rotate(-M_PI/2, 0, 1, 0);

        rotation_ += 0.01;
    }
};

class DuckBehaviour : public tsn::Behaviour {
    static std::uniform_int_distribution<> face_dist_;

    const float speed_ = 0.01;
    const float min_scale = 2;
    const int bounce_face_;

    int hits_left_;

public:
    DuckBehaviour(int hits_left)
     : hits_left_{hits_left}, bounce_face_{face_dist_(duck_random_generator())}
    { }

    auto hits_left() const noexcept {return hits_left_;}

    auto scale() const noexcept {
        return std::pow(min_scale, hits_left_);
    }

    void update(
        tsn::Scene *scene,
        simd_float4x4 global_transform,
        tsn::Node *node,
        tsn::time_point time,
        const std::vector<tsn::Node*>& collisions)
    override {
        auto hit_by_bullet = std::any_of(collisions.cbegin(), collisions.cend(), [](auto n) {
            return dynamic_cast<BulletBehaviour*>(n->behaviour().get()) != nullptr;
        });

        if (hit_by_bullet) {
            scene->root()->delete_child(node);
        }

        auto border = 14;
        auto [x, y, z] = get_translation(global_transform);
        if (x < -border || x > border) {
            auto mirror = simd_float4x4{{
                { -1,  0,  0,  0 },
                {  0,  1,  0,  0 },
                {  0,  0,  1,  0 },
                {  0,  0,  0,  1 }
            }};
            auto m = simd_mul(mirror, node->transform.matrix());
            m.columns[3] = {x, y, z, 1};
            node->transform.set(m);
        }

        if (z < -2 * border || z > border) {
            auto mirror = simd_float4x4{{
                {  1,  0,  0,  0 },
                {  0,  1,  0,  0 },
                {  0,  0, -1,  0 },
                {  0,  0,  0,  1 }
            }};
            auto m = simd_mul(mirror, node->transform.matrix());
            m.columns[3] = {x, y, z, 1};
            node->transform.set(m);
        }

        auto ms = std::chrono::time_point_cast<std::chrono::milliseconds>(time).time_since_epoch().count();
        node->transform.translate(-speed_, 0.002 * sin((ms - bounce_face_)/300.0f), 0);
    }
};

std::uniform_int_distribution<int> DuckBehaviour::face_dist_{0, 999};


class MoonBehaviour : public tsn::Behaviour {
    const float radius_;
    const float scale_;

    float rotation_;

public:
    MoonBehaviour(float scale, float radius, float rotation = 0)
     : scale_{scale}, radius_{radius}, rotation_{rotation}
    { }

    void update(
        tsn::Scene *scene,
        simd_float4x4 global_transform,
        tsn::Node *node,
        tsn::time_point time,
        const std::vector<std::pair<tsn::Node*, tsn::Node*>>& collisions)
    override {
        node->transform.reset()
            .rotate(rotation_, 0, 1, 0)
            .translate(radius_, 0, 0)
            .scale(scale_)
            .rotate(M_PI, 0, 1, 0)
            .rotate(10 * rotation_, 0, 0, 1);

        rotation_ += 0.03;
    }
};

class BulletBehaviour : public tsn::Behaviour {
    const std::shared_ptr<tsn::metal::AssetLibrary> lib_;
//    tsn::time_point start_;

public:
    BulletBehaviour(std::shared_ptr<tsn::metal::AssetLibrary> lib)
     : lib_{lib}
    { }

    void update(
        tsn::Scene *scene,
        simd_float4x4 global_transform,
        tsn::Node *node,
        tsn::time_point time,
        const std::vector<tsn::Node*>& collisions)
    override {
        for (auto n : collisions) {
            std::cout << "BOOM: " << n->name() << std::endl;
        }

        auto duck = std::find_if(collisions.cbegin(), collisions.cend(), [](auto n) {
            return dynamic_cast<DuckBehaviour*>(n->behaviour().get()) != nullptr;
        });

        auto hit_duck = duck != collisions.cend();

        if (hit_duck) {
            auto duck_behaviour = dynamic_cast<DuckBehaviour*>((*duck)->behaviour().get());
            auto hits_left = duck_behaviour->hits_left();

            scene->root()->delete_child(node);

            if (--hits_left > 0) {
                auto m = node->transform.matrix();
                auto tm = simd_transpose(m);
                auto vx = simd_make_float3(tm.columns[0]);
                auto vy = simd_make_float3(tm.columns[1]);
                auto vz = simd_make_float3(tm.columns[2]);

                auto sx = simd_length(vx);
                auto sy = simd_length(vy);
                auto sz = simd_length(vz);

                std::cout << "BOOM: " << node->name() << " " << sx << " " << sy << " " << sz << std::endl;

                vx /= sx;
                vy /= sy;
                vz /= sz;

                tm.columns[0] = simd_make_float4(vx, tm.columns[0][3]);
                tm.columns[1] = simd_make_float4(vy, tm.columns[1][3]);
                tm.columns[2] = simd_make_float4(vz, tm.columns[2][3]);

                m = simd_transpose(tm);

                auto b = std::make_shared<DuckBehaviour>(hits_left);
                auto left_duck = lib_->make_node("duckling", 0.5, tsn::metal::AssetKind::Bob, b);
                left_duck->transform.set(m).scale(b->scale());
                scene->root()->add_child(std::move(left_duck));

                auto right_duck = lib_->make_node("duckling", 0.5, tsn::metal::AssetKind::Bob, std::make_shared<DuckBehaviour>(hits_left));
                right_duck->transform.set(m).scale(b->scale()).rotate(M_PI, 0, 1, 0);
                scene->root()->add_child(std::move(right_duck));
            }
        }

//        if (time - start_ > 5s) {
//            scene->root()->delete_child(node);
//        }

        auto border = 14;
        auto [x, y, z] = get_translation(global_transform);
        if (x < -border || x > border) {
            scene->root()->delete_child(node);
        }
        else if (z < -2 * border || z > border) {
            scene->root()->delete_child(node);
        }
        else {
            node->transform.translate(0, 0, 0.3);
        }
    }
};

class PlayerBehaviour : public tsn::Behaviour {
    const std::shared_ptr<tsn::metal::AssetLibrary> lib_;

    float target_speed_;
    float speed_;

    tsn::time_point time_last_shot_;

public:
    PlayerBehaviour(std::shared_ptr<tsn::metal::AssetLibrary> lib)
     : lib_{std::move(lib)}, target_speed_{0}, speed_{0}, time_last_shot_{}
    { }

    void update(
        tsn::Scene *scene,
        simd_float4x4 global_transform,
        tsn::Node *node,
        tsn::time_point time,
        const std::vector<tsn::Node*>& collisions)
    override {
//        auto pos = get_translation(global_transform);
//        std::cout << "px=" << pos[0] << " py=" << pos[1] << " pz=" << pos[2] << std::endl;

        auto& input = tsn::input_instance();

        target_speed_ = 0;
        if (input.is_pressed(126)) {
            target_speed_ = -0.05;
        }

        //            if (input.is_pressed(125)) {
        //                target_speed = 0;
        //            }

        const auto alpha = 0.1f;
        speed_ = alpha * target_speed_ + (1 - alpha) * speed_;
        node->transform.translate(0, 0, -speed_);

        if (input.is_pressed(123)) {
            node->transform.rotate(0.05, 0, 1, 0);
        }

        if (input.is_pressed(124)) {
            node->transform.rotate(-0.05, 0, 1, 0);
        }

        if (input.is_pressed(49)) {
            if (time - time_last_shot_ >= 300ms) {
                auto bullet = lib_->make_node("bullet", 0.5, tsn::metal::AssetKind::Blub, std::make_shared<BulletBehaviour>(lib_));

                time_last_shot_ = time;
                bullet->transform = node->transform;
                bullet->transform.translate(0, 0, 0.5).scale(0.7);//.rotate(-M_PI/2, 0, 1, 0);
                scene->root()->add_child(std::move(bullet));
            }
        }
    }
};

std::unique_ptr<tsn::Scene> build_scene(std::shared_ptr<tsn::metal::AssetLibrary> lib) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> random{0, 2 * M_PI};
    std::uniform_real_distribution<> random_pos{-10, 10};

    auto root = std::shared_ptr<tsn::Node>{lib->make_node("root")};

    auto planet0 = lib->make_node("planet", 0.5, tsn::metal::AssetKind::Bob, std::make_shared<PlanetBehaviour>(4, 12.5, M_PI));
    planet0->add_child(lib->make_node("moon", 0.2, tsn::metal::AssetKind::Blub, std::make_shared<MoonBehaviour>(0.5, 1)));

    auto player = lib->make_node("player", 0.5, tsn::metal::AssetKind::Blub, std::make_shared<PlayerBehaviour>(lib));
    player->transform.rotate(M_PI/2*0, 0, 1, 0).scale(5);

    const auto N = 0;
    //        std::vector<float> floats;
    //        std::generate_n(std::back_inserter(floats), N, [i = 0]() mutable {
    //            return static_cast<float>(i++ * 2 * M_PI / N);
    //        });
    //        for (auto f : floats) {
    //            player->add_child(lib->make_node(tsn::metal::AssetKind::Blub, update_func(0.4, 0.8, f)));
    //        }

    for (auto i = 0; i < N; ++i) {
        player->add_child(lib->make_node("pmoon " + std::to_string(i), 0.2, tsn::metal::AssetKind::Blub, std::make_shared<MoonBehaviour>(0.4, 0.8, i * 2 * M_PI / N)));
    }

    for (auto i = 0; i < 4; ++i) {
        auto b = std::make_shared<DuckBehaviour>(3);
        auto duck = lib->make_node("duck:" + std::to_string(i), 1, tsn::metal::AssetKind::Bob, b);
        auto angle = random(gen);
        duck->transform.translate(random_pos(gen), 0, random_pos(gen)).scale(b->scale()).rotate(angle, 0, 1, 0);
        root->add_child(std::move(duck));
    }

//    root->add_child(std::move(planet0));
    root->add_child(std::move(player));

    return std::make_unique<tsn::Scene>(std::move(root), std::vector{
        Light{simd_float3{ 5,  5, 0}, simd_float3{0.3, 0.3, 0.3}},
        Light{simd_float3{-5,  5, 0}, simd_float3{0.3, 0.3, 0.3}},
        Light{simd_float3{ 0, -5, 0}, simd_float3{0.3, 0.3, 0.3}},
    });
}
}

