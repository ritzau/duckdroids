//
//  ShaderTypes.hpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2021-01-03.
//

#pragma once

#include <simd/simd.h>

enum class BufferIndex : unsigned long
{
    BufferIndexMeshPositions = 0,
    BufferIndexMeshGenerics  = 1,
    BufferIndexUniforms      = 2,
};

enum class VertexAttribute : unsigned long
{
    VertexAttributePosition  = 0,
    VertexAttributeNormal    = 1,
    VertexAttributeTexcoord  = 2,
};

enum class TextureIndex : unsigned long
{
    TextureIndexColor    = 0,
};

struct VertexUniforms {
    simd_float4x4 viewProjectionMatrix;
    simd_float4x4 modelMatrix;
    simd_float3x3 normalMatrix;
};

#define LightCount 3

struct Light {
    simd_float3 worldPosition;
    simd_float3 color;
};

struct FragmentUniforms {
    simd_float3 cameraWorldPosition;
    simd_float3 ambientLightColor;
    simd_float3 specularColor;
    float specularPower;
    Light lights[LightCount];
};
