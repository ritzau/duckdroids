//
//  MetalNode.m
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-24.
//

#include <type_traits>

#import <Foundation/Foundation.h>
#import <simd/simd.h>
#import <MetalKit/MetalKit.h>
#import <ModelIO/ModelIO.h>

#import "MetalNode.h"

#import "Matrix.h"
#import "Scene.hpp"

namespace tsn {
    namespace metal {

        template <typename E>
        constexpr auto to_underlying(E e) noexcept
        {
            return static_cast<std::underlying_type_t<E>>(e);
        }

        MetalAssetLibrary::MetalAssetLibrary(
              MetalDevice device,
              MTKMeshBufferAllocator *metalAllocator,
              MDLVertexDescriptor *mdlVertexDescriptor,
              MTKTextureLoader *texture_loader)
         : device_{device},
           metalAllocator_{metalAllocator},
           mdlVertexDescriptor_{mdlVertexDescriptor},
           texture_loader_{texture_loader}
        { }

        MTKMesh *MetalAssetLibrary::load_mesh(NSURL *url) {
            NSError *error = nullptr;

            auto asset = [[MDLAsset alloc] initWithURL:url vertexDescriptor:mdlVertexDescriptor_ bufferAllocator:metalAllocator_];
            NSLog(@"Bob %@", asset);

            auto meshes = [MTKMesh newMeshesFromAsset:asset device:device_ sourceMeshes:nil error:&error];

            if(!meshes || error)
            {
                NSLog(@"Error loading Bob %@", error.localizedDescription);
            }

            return meshes[0];
        }

        MetalTexture MetalAssetLibrary::load_texture(NSString *name) {
            NSError *error = nullptr;

            auto textureLoader = [[MTKTextureLoader alloc] initWithDevice:device_];

            NSDictionary *textureLoaderOptions =
            @{
                MTKTextureLoaderOptionTextureUsage       : @(MTLTextureUsageShaderRead),
                MTKTextureLoaderOptionTextureStorageMode : @(MTLStorageModePrivate)
            };


            auto color_map = [textureLoader newTextureWithName:name
                                              scaleFactor:1.0
                                                   bundle:nil
                                                  options:textureLoaderOptions
                                                    error:&error];

            if(!color_map || error)
            {
                NSLog(@"Error creating texture %@", error.localizedDescription);
            }

            return color_map;
        }


        MetalNode::MetalNode(std::string name, float collision_radius, id<MTLTexture> texture, MTKMesh *mesh, std::shared_ptr<Behaviour> behaviour)
            : material_{texture}, mesh_{mesh}, Node(name, collision_radius, std::move(behaviour)) {
        }

        MetalNode::MetalNode() : MetalNode("empty", 0.0f, nullptr, nullptr, std::make_shared<Behaviour>()) {
        }

        void MetalNode::render(Scene* scene, id<MTLRenderCommandEncoder> renderEncoder, matrix_float4x4 parent_transform) {
            auto model_matrix = matrix_multiply(parent_transform, transform.matrix());

            for (auto& child : children_) {
                dynamic_cast<MetalNode*>(child.get())->render(scene, renderEncoder, model_matrix);
            }

            if((NSNull*)mesh_ == [NSNull null]) {
                return;
            }

            auto viewProjectionMatrix = simd_mul(scene->projection_matrix(), scene->view_matrix());

            auto vertexUniforms = VertexUniforms {
                viewProjectionMatrix,
                transform.matrix(),
                normal_matrix_of(transform.matrix()),
            };

            [renderEncoder setVertexBytes:&vertexUniforms length:sizeof(vertexUniforms) atIndex:to_underlying(BufferIndex::BufferIndexMeshGenerics)];

            auto fragmentUniforms = FragmentUniforms{
                scene->camera_world_position(),
                scene->ambient_light_color(),
                material_.specularColor,
                material_.specularPower,
                scene->lights_[0],
                scene->lights_[1],
                scene->lights_[2],
            };

            [renderEncoder setFragmentBytes:&fragmentUniforms length:sizeof(fragmentUniforms) atIndex:to_underlying(BufferIndex::BufferIndexMeshPositions)];

            [renderEncoder pushDebugGroup:@"DrawBox"];

            [renderEncoder setFragmentTexture:material_.baseColorTexture
                                      atIndex:to_underlying(TextureIndex::TextureIndexColor)];

            MTKMeshBuffer *vertexBuffer = mesh_.vertexBuffers[0];
            [renderEncoder setVertexBuffer:vertexBuffer.buffer offset:vertexBuffer.offset atIndex: 0];

            for (MTKSubmesh *submesh in mesh_.submeshes)
            {
                [renderEncoder drawIndexedPrimitives:submesh.primitiveType
                                          indexCount:submesh.indexCount
                                           indexType:submesh.indexType
                                         indexBuffer:submesh.indexBuffer.buffer
                                   indexBufferOffset:submesh.indexBuffer.offset];
            }

            [renderEncoder popDebugGroup];
        }
    }
}
