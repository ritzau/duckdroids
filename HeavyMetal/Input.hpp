//
//  Input.hpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-30.
//

#ifndef Input_hpp
#define Input_hpp

#include <set>

namespace tsn {

    class Input {
        std::set<int> pressed_keys_;
        
    public:
        void press_key(int key_code);
        void release_key(int key_code);

        bool is_pressed(int key_code);
    };

    Input& input_instance();
}

#endif /* Input_hpp */
