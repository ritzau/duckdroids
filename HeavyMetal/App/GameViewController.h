//
//  GameViewController.h
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-19.
//

#import <Cocoa/Cocoa.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import "Renderer.h"

// Our macOS view controller.
@interface GameViewController : NSViewController

@end
