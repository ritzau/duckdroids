//
//  GameView.m
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-28.
//

#include "Input.hpp"

#import "GameView.h"

@implementation GameView

- (void)keyUp:(NSEvent *)theEvent
{
    tsn::input_instance().release_key(theEvent.keyCode);
}

- (void)keyDown:(NSEvent *)theEvent
{
    if (!theEvent.isARepeat) {
        tsn::input_instance().press_key(theEvent.keyCode);
    }
}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

@end
