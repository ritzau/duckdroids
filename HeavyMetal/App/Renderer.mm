//
//  Renderer.m
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-19.
//

#include <chrono>
#include <cmath>

#include "ShaderTypes.hpp"


#import <simd/simd.h>
#import <ModelIO/ModelIO.h>

#import "Renderer.h"

#include "Duckroids.hpp"
#include "Input.hpp"
#include "Scene.hpp"
#include "MetalNode.h"
#include "ShaderTypes.hpp"

static const NSUInteger kMaxBuffersInFlight = 3;

template <typename E>
constexpr auto to_underlying(E e) noexcept
{
    return static_cast<std::underlying_type_t<E>>(e);
}

@implementation Renderer
{
    dispatch_semaphore_t _inFlightSemaphore;
    id <MTLDevice> _device;
    id <MTLCommandQueue> _commandQueue;

    id <MTLRenderPipelineState> _pipelineState;
    id <MTLDepthStencilState> _depthState;
    id <MTLSamplerState> _samplerState;

    MTKMeshBufferAllocator *_metalAllocator;
    MDLVertexDescriptor *_mdlVertexDescriptor;

    uint32_t _uniformBufferOffset;

    uint8_t _uniformBufferIndex;

    std::unique_ptr<tsn::Scene> _scene;
}

-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view;
{
    self = [super init];
    if(self)
    {
        _device = view.device;
        _inFlightSemaphore = dispatch_semaphore_create(kMaxBuffersInFlight);
        [self _loadMetalWithView:view];
        [self _loadAssets];
    }

    return self;
}

- (void)_loadMetalWithView:(nonnull MTKView *)view;
{
    /// Load Metal state objects and initialize renderer dependent view properties

    view.depthStencilPixelFormat = MTLPixelFormatDepth32Float_Stencil8;
    view.colorPixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
    view.sampleCount = 1;

    _mdlVertexDescriptor = [Renderer _buildVertexDescriptor];
    _samplerState = [Renderer buildSamplerStateWithDevice:_device];
    _depthState = [Renderer buildDepthStencilStateWithDevice:_device];
    _pipelineState = [Renderer _buildPipelineWithDecive:_device view:view vertexDescriptor:_mdlVertexDescriptor];
    _commandQueue = [_device newCommandQueue];
}

+ (id <MTLRenderPipelineState>)_buildPipelineWithDecive:(id <MTLDevice>) device view:(MTKView*) view vertexDescriptor:(MDLVertexDescriptor*) vertexDescriptor
{
    auto library = [device newDefaultLibrary];
    if (!library)
    {
        NSLog(@"Could not load default library from main bundle");
        exit(5);
    }

    auto vertex_function = [library newFunctionWithName:@"vertex_main"];
    auto fragment_function = [library newFunctionWithName:@"fragment_main"];

    auto pipeline_descriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipeline_descriptor.label = @"MyPipeline";
    pipeline_descriptor.sampleCount = view.sampleCount;
    pipeline_descriptor.vertexFunction = vertex_function;
    pipeline_descriptor.fragmentFunction = fragment_function;
    pipeline_descriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(vertexDescriptor);
    pipeline_descriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
    pipeline_descriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat;
    pipeline_descriptor.stencilAttachmentPixelFormat = view.depthStencilPixelFormat;

    NSError *error = nullptr;
    auto pipeline_state = [device newRenderPipelineStateWithDescriptor:pipeline_descriptor error:&error];
    if (!pipeline_state || error)
    {
        NSLog(@"Could not create render pipeline state object: %@", error);
        exit(5);
    }

    return pipeline_state;
}


+ (MDLVertexDescriptor*)_buildVertexDescriptor
{
    auto mdlVertexDescriptor = [[MDLVertexDescriptor alloc] init];

    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributePosition)].name = MDLVertexAttributePosition;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributePosition)].format = MDLVertexFormatFloat3;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributePosition)].offset = 0;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributePosition)].bufferIndex = to_underlying(BufferIndex::BufferIndexMeshPositions);

    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeNormal)].name = MDLVertexAttributeNormal;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeNormal)].format = MDLVertexFormatFloat3;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeNormal)].offset = 3 * sizeof(float);
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeNormal)].bufferIndex = to_underlying(BufferIndex::BufferIndexMeshPositions);

    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeTexcoord)].name = MDLVertexAttributeTextureCoordinate;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeTexcoord)].format = MDLVertexFormatFloat2;
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeTexcoord)].offset = 6 * sizeof(float);
    mdlVertexDescriptor.attributes[to_underlying(VertexAttribute::VertexAttributeTexcoord)].bufferIndex = to_underlying(BufferIndex::BufferIndexMeshPositions);

    mdlVertexDescriptor.layouts[to_underlying(BufferIndex::BufferIndexMeshPositions)].stride = 8 * sizeof(float);

    return mdlVertexDescriptor;
}

+ (id<MTLSamplerState>) buildSamplerStateWithDevice:(id<MTLDevice>) device
{
    auto samplerDescriptor = [[MTLSamplerDescriptor alloc] init];
    samplerDescriptor.normalizedCoordinates = true;
    samplerDescriptor.minFilter = MTLSamplerMinMagFilterLinear;
    samplerDescriptor.magFilter = MTLSamplerMinMagFilterLinear;
    samplerDescriptor.mipFilter = MTLSamplerMipFilterLinear;
    return [device newSamplerStateWithDescriptor: samplerDescriptor];
}

+ (id<MTLDepthStencilState>) buildDepthStencilStateWithDevice:(id<MTLDevice>) device
{
    auto depthStencilDescriptor = [[MTLDepthStencilDescriptor alloc] init];
    depthStencilDescriptor.depthCompareFunction = MTLCompareFunctionLess;
    depthStencilDescriptor.depthWriteEnabled = true;
    return [device newDepthStencilStateWithDescriptor:depthStencilDescriptor];
}

- (void)_loadAssets
{
    _metalAllocator = [[MTKMeshBufferAllocator alloc] initWithDevice: _device];

    auto metal_asset_lib = new tsn::metal::MetalAssetLibrary{_device, _metalAllocator, _mdlVertexDescriptor, nullptr};
    auto asset_lib = std::make_shared<tsn::metal::AssetLibrary>(metal_asset_lib);
    _scene = duck::build_scene(asset_lib);
}

- (void)drawInMTKView:(nonnull MTKView *)view
{
    _scene->update(std::chrono::high_resolution_clock::now());

    dispatch_semaphore_wait(_inFlightSemaphore, DISPATCH_TIME_FOREVER);

    id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    commandBuffer.label = @"MyCommand";

    __block dispatch_semaphore_t block_sema = _inFlightSemaphore;
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer)
     {
        dispatch_semaphore_signal(block_sema);
    }];

    /// Delay getting the currentRenderPassDescriptor until we absolutely need it to avoid
    ///   holding onto the drawable and blocking the display pipeline any longer than necessary
    MTLRenderPassDescriptor* renderPassDescriptor = view.currentRenderPassDescriptor;

    if(renderPassDescriptor != nil) {

        /// Final pass rendering code here
        [renderPassDescriptor colorAttachments][0].clearColor = MTLClearColorMake(0.63, 0.81, 1.0, 1.0);

        id <MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
        renderEncoder.label = @"MyRenderEncoder";

        if (_scene) {
            [renderEncoder setFrontFacingWinding:MTLWindingCounterClockwise];
//            [renderEncoder setCullMode:MTLCullModeBack];
            [renderEncoder setRenderPipelineState:_pipelineState];
            [renderEncoder setDepthStencilState:_depthState];
            [renderEncoder setFragmentSamplerState:_samplerState atIndex:0];

            _scene->render<tsn::metal::MetalNode>(renderEncoder);
        }
        
        [renderEncoder endEncoding];

        [commandBuffer presentDrawable:view.currentDrawable];
    }

    [commandBuffer commit];
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
    if (_scene) {
        _scene->update_dimensions(size.width, size.height);
    }
}

@end
