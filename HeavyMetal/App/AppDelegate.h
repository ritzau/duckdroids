//
//  AppDelegate.h
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-19.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
