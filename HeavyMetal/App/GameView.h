//
//  GameView.h
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-28.
//

#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GameView : MTKView

@end

NS_ASSUME_NONNULL_END
