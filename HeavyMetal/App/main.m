//
//  main.m
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-19.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
