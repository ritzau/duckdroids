//
//  MetalNode.h
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-24.
//

#ifndef MetalNode_h
#define MetalNode_h

#include <functional>

#include "ShaderTypes.hpp"


#import <Foundation/Foundation.h>
#import <simd/simd.h>
#import <MetalKit/MetalKit.h>
#import <ModelIO/ModelIO.h>

#import "Matrix.h"
#import "Scene.hpp"


namespace tsn {
    namespace metal {

        using MetalDevice = id<MTLDevice>;
        using MetalTexture = id<MTLTexture>;

        struct Material {
            simd_float3 specularColor = simd_float3{1, 1, 1};
            float specularPower = 20;
            MetalTexture baseColorTexture = nullptr;

            Material(MetalTexture texture) : baseColorTexture{texture}
            { }
        };

        class MetalAssetLibrary {
            MetalDevice device_;
            MTKMeshBufferAllocator *metalAllocator_;
            MDLVertexDescriptor *mdlVertexDescriptor_;
            MTKTextureLoader *texture_loader_;
            
        public:
            MetalAssetLibrary(
                  MetalDevice device,
                  MTKMeshBufferAllocator *metalAllocator,
                  MDLVertexDescriptor *mdlVertexDescriptor,
                  MTKTextureLoader *texture_loader);

            MTKMesh *load_mesh(NSURL *url);
            MetalTexture load_texture(NSString *name);
        };

        class MetalNode : public tsn::Node {
            Material material_;
            MTKMesh *mesh_;

        public:
            MetalNode(std::string name, float collision_radius, id<MTLTexture> texture, MTKMesh *mesh, std::shared_ptr<Behaviour> behaviour);
            MetalNode();

            void render(Scene *scene, id<MTLRenderCommandEncoder> renderEncoder, matrix_float4x4 parent_tranform = matrix_identity_float4x4);
        };
    }
}

#endif /* MetalNode_h */
