//
//  MetalNodeBuilder.m
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-30.
//

#include <map>
#include <string>
#include <tuple>

#import <Foundation/Foundation.h>

#include "MetalNodeBuilder.h"
#include "MetalNode.h"

using namespace std::string_literals;

namespace tsn {
namespace metal {

static std::map<AssetKind, std::tuple<MTKMesh*, MetalTexture>> mesh_cache_;

static std::tuple<NSURL*, NSString*> get_asset_descriptor(AssetKind asset) {
    switch (asset) {
    case AssetKind::Bob: return {[[NSBundle mainBundle] URLForResource:@"bob" withExtension:@"obj"], @"bob_baseColor"};
    case AssetKind::Blub: return {[[NSBundle mainBundle] URLForResource:@"blub" withExtension:@"obj"], @"blub_baseColor"};
    default: throw std::logic_error{"Unexpected asset: "s + std::to_string(static_cast<int>(asset))};
    }
}

static std::tuple<MTKMesh*, MetalTexture> get_material(MetalAssetLibrary *metal_library_, AssetKind asset_kind) {
    auto [asset_url, texture_name] = get_asset_descriptor(asset_kind);

    if (mesh_cache_.count(asset_kind) > 0) {
        auto [mesh, texture] = mesh_cache_[asset_kind];
        return {mesh, texture};
    }
    else {
        auto mesh = metal_library_->load_mesh(asset_url);
        auto texture = metal_library_->load_texture(texture_name);
        mesh_cache_[asset_kind] = {mesh, texture};
        return {mesh, texture};
    }
}

std::unique_ptr<Node> AssetLibrary::make_node(std::string name, float collision_radius, AssetKind asset_kind, std::shared_ptr<Behaviour> behaviour) {
    if (asset_kind == AssetKind::None) {
        return std::make_unique<MetalNode>();
    }

    auto [mesh, texture] = get_material(metal_library_, asset_kind);
    return std::make_unique<MetalNode>(name, collision_radius, texture, mesh, behaviour);
}

std::unique_ptr<Node> AssetLibrary::make_node(std::string name, float collision_radius, AssetKind asset) {
    return make_node(name, collision_radius, asset, std::make_shared<Behaviour>());
}

}
}
