//
//  Matrix.h
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-25.
//

#ifndef Matrix_h
#define Matrix_h

#include <simd/simd.h>

#ifdef __cplusplus
    extern "C" {
#endif

    matrix_float4x4 matrix4x4_translation(float tx, float ty, float tz);
    matrix_float4x4 matrix4x4_scale(float sx, float sy, float sz);
    matrix_float4x4 matrix4x4_rotation(float radians, vector_float3 axis);
    matrix_float4x4 matrix_perspective_right_hand(float fovyRadians, float aspect, float nearZ, float farZ);

    simd_float3 get_translation(simd_float4x4 matrix);
    simd_float3x3 normal_matrix_of(simd_float4x4 matrix);

#ifdef __cplusplus
    }
#endif


#endif /* Matrix_h */
