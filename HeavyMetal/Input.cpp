//
//  Input.cpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-30.
//

#include <iostream>

#include "Input.hpp"

namespace tsn {
void Input::press_key(int key_code) {
    pressed_keys_.insert(key_code);
//    std::cout << "pressed " << key_code << std::endl;
}

void Input::release_key(int key_code) {
    pressed_keys_.erase(key_code);
}

bool Input::is_pressed(int key_code) {
    return pressed_keys_.find(key_code) != pressed_keys_.end();
}

Input& input_instance() {
    static Input input;
    return input;
}

}
