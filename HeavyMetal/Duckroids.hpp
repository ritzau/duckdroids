//
//  Duckroids.hpp
//  HeavyMetal
//
//  Created by Tobias Ritzau on 2020-12-30.
//

#pragma once

#include <memory>

#include "Scene.hpp"
#include "MetalNodeBuilder.h"

namespace duck {

std::unique_ptr<tsn::Scene> build_scene(std::shared_ptr<tsn::metal::AssetLibrary> lib);

}
